package com.example.kotlin_13


import androidx.lifecycle.ViewModel


import android.app.Activity
import android.content.Intent
import android.net.Uri
import com.example.kotlin_13.connection.ConnectionMessageSingleton
import com.example.kotlin_13.data.FirebaseClient
import com.example.kotlin_13.repositories.CalendarRepository
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore

import java.util.Calendar


class ViewModelCalendar : ViewModel () {

    private val repository = CalendarRepository()

    fun realizarPago(calendar: Calendar) {
        repository.realizarPago(calendar)
    }


    fun volverAPaymentActivity(activity: Activity) {
        val intent = Intent(activity, PaymentActivity::class.java)
        activity.startActivity(intent)
    }
}

