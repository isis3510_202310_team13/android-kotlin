package com.example.kotlin_13.connection

import javax.inject.Singleton

@Singleton
object ConnectionMessageSingleton {

    private var showSignup = false
    private var showSignin = false
    private var showProfile = false


    fun showSignUp() {
        showSignup = true
    }

    fun getShowSignUp(): Boolean {
        return showSignup
    }

    fun restartShowSignUp(){
        showSignup = false
    }

    fun showSignIn() {
        showSignin = true
    }

    fun getShowSignIn(): Boolean {
        return showSignin
    }

    fun restartShowSignIn(){
        showSignin = false
    }

    fun showProfile() {
        showProfile = true
    }

    fun getShowProfile(): Boolean {
        return showProfile
    }

    fun restartShowProfile(){
        showProfile = false
    }

}