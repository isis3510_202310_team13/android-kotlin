package com.example.kotlin_13.auth

import android.app.AlertDialog
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import com.example.kotlin_13.MainActivity
import com.example.kotlin_13.R
import com.example.kotlin_13.caching.UserPreferences
import com.example.kotlin_13.connection.ConnectionMessageSingleton
import com.example.kotlin_13.connection.NetworkConnectivityObserver
import com.example.kotlin_13.data.FirebaseClient
import com.google.firebase.auth.FirebaseAuth
import com.example.kotlin_13.databinding.ActivityLoginBinding
import com.example.kotlin_13.repositories.UserRepository
import com.example.kotlin_13.viewmodels.UserViewModel

class SignInActivity : AppCompatActivity() {

    private lateinit var binding: ActivityLoginBinding
    private lateinit var firebaseAuth: FirebaseAuth

    private lateinit var builder: AlertDialog.Builder

    private val userRepository = UserRepository()
    private val viewModel = UserViewModel(userRepository)

    private val connectionMessages= ConnectionMessageSingleton



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)



        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);

        builder = AlertDialog.Builder(this)

        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // Auth service
        firebaseAuth = FirebaseClient.getAuthInstance()

        val networkConnectivityReceiver = NetworkConnectivityObserver(applicationContext)
        networkConnectivityReceiver.observe(this, androidx.lifecycle.Observer { isConnected ->
            if(isConnected){
                if(connectionMessages.getShowSignIn() === true){
                    if(firebaseAuth.currentUser != null){
                        firebaseAuth.signOut()
                    }
                    builder.setMessage("Internet is back!")
                        .setPositiveButton("Refresh"){dialogInterface,it ->
                            connectionMessages.restartShowSignIn()

                            recreate()
                        }
                        .show()

                }

                // If user is logged, goes directly to MainActivity
                if(firebaseAuth.currentUser != null){
                    val intent = Intent(this, MainActivity::class.java)
                    startActivity(intent)
                }

            }
            else{

                if(connectionMessages.getShowSignIn() === false){
                    builder.setMessage("No Internet connection. Login once the connection is back")
                        .setPositiveButton("OK"){dialogInterface,it ->
                            dialogInterface.cancel()
                        }
                        .show()
                    connectionMessages.showSignIn()

                }

                binding.btnLogin.isEnabled = false
                binding.btnLogin.setBackgroundColor(Color.GRAY)
            }
        })

        viewModel.isSignInSuccessful.observe(this) { isSuccess ->
            if (isSuccess) {
                // Redirects to signInActivity, but because it is logged, it goes to MainActivity
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
            } else {
                // El registro falló, realiza alguna acción aquí
                builder.setMessage("Incorrect credentials")
                    .setPositiveButton("Cancel"){dialogInterface,it ->
                        val intent = Intent(this, SignInActivity::class.java)
                        startActivity(intent)                    }
                    .show()
            }
        }

        // Register Button, opens SignUpActivity
        binding.registerButton.setOnClickListener {
            val intent = Intent(this, SignUpActivity::class.java)
            startActivity(intent)
        }

        // Login Button when email and password spaces are filled
        binding.btnLogin.setOnClickListener {
            val email = binding.emailEt.text.toString()
            val pass = binding.passET.text.toString()

            viewModel.signIn(email,pass,builder)


        }
    }

    override fun onStart() {
        super.onStart()

    }
}