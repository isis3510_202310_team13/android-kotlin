package com.example.kotlin_13

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.RecyclerView
import MenuAdapter
import android.content.Intent
import androidx.appcompat.app.AppCompatDelegate
import com.example.kotlin_13.databinding.ActivityMenuListBinding
import com.example.kotlin_13.model.MenuItem
import com.google.firebase.firestore.FirebaseFirestore

class MenuListActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMenuListBinding
    private lateinit var recyclerView: RecyclerView
    private lateinit var menuAdapter: MenuAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu_list)
        binding = ActivityMenuListBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val db = FirebaseFirestore.getInstance()
        val collectionRef = db.collection("menus")
        val myRecyclerView = findViewById<RecyclerView>(R.id.recycler_view)
        collectionRef.get().addOnSuccessListener { querySnapshot ->
            val dataList = ArrayList<MenuItem>()

            for (document in querySnapshot.documents) {
                val data = document.toObject(MenuItem::class.java)
                data?.let {
                    dataList.add(it)
                }
            }

            val adapter = MenuAdapter(dataList)
            myRecyclerView.adapter = adapter
        }

        // Button to go back to MainActivity
        binding.backBtn.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }
        binding.btnNt.setOnClickListener {
            val intent = Intent(this, PaymentActivity::class.java)
            startActivity(intent)
        }



    }
}
