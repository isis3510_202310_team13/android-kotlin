package com.example.kotlin_13.caching

import android.content.Context
import android.content.SharedPreferences
import android.net.Uri
import com.example.kotlin_13.entities.User
import javax.inject.Singleton

@Singleton
object UserPreferences : SharedPreferences.OnSharedPreferenceChangeListener {

    private const val PREF_NAME = "UserPreferences"
    private const val USER_ID = "user_id"
    private const val USER_NAME = "user_name"
    private const val USER_EMAIL = "user_email"
    private const val USER_STATUS = "user_status"
    private const val USER_PHOTO = "user_photo"
    private const val USER_URI = "user_uri"

    private lateinit var preferences: SharedPreferences

    fun init(context: Context) {
        preferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        preferences.registerOnSharedPreferenceChangeListener(this)
    }

    fun saveUserData(user: User) {
        with(preferences.edit()) {
            putString(USER_ID, user.id)
            putString(USER_NAME, user.name)
            putString(USER_EMAIL, user.email)
            putString(USER_STATUS, user.status)
            putString(USER_PHOTO, user.photo)
            apply()
        }
    }

    fun deleteData(){
        val editor = preferences.edit()
        editor.clear()
        editor.apply()
    }


    fun getUserId(): String? {
        return preferences.getString(USER_ID, null)
    }

    fun setName(name:String){
        with(preferences.edit()) {
            putString(USER_NAME, name)
            apply()
        }
    }

    fun getUserName(): String? {
        return preferences.getString(USER_NAME, null)
    }

    fun setEmail(email:String){
        with(preferences.edit()) {
            putString(USER_EMAIL, email)
            apply()
        }
    }
    fun getUserEmail(): String? {
        return preferences.getString(USER_EMAIL, null)
    }

    fun getUserStatus(): String? {
        return preferences.getString(USER_STATUS, null)
    }

    fun setPhoto(url:String){
        with(preferences.edit()) {
            putString(USER_PHOTO, url)
            apply()
        }
    }
    fun getUserPhoto(): String? {
        return preferences.getString(USER_PHOTO, null)
    }

    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences?, key: String?) {
        // Handle changes in SharedPreferences data here
    }
}