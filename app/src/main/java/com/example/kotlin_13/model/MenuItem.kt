package com.example.kotlin_13.model

data class MenuItem(
    val name: String = "",
    val price: Int = 0,
    val description: String = "",
    val photoURL: String = ""
){
    constructor() : this("", 0, "", "")
}
